# GSL

**Requisitos:**

* npm

**Como executar:**

* Carregar o arquivo data/entregas.sql em uma base de dados MySQL.
* Atualizar o arquivo lib/db.js com o endereço e credenciais de acesso ao banco.
* Executar o comando npm install
* Acessar através de localhost:3000/
* Credenciais de acesso
  - usuário: admin
  - senha: admin