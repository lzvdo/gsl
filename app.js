var createError = require('http-errors');
var express = require('express');
var path = require('path');
var logger = require('morgan');

var flash = require('express-flash');
var session = require('express-session');

// Routes
var indexRouter = require('./routes/');
var dashboardRouter = require('./routes/dashboard');
var trackRouter = require('./routes/track');

// Services
var trackingService = require('./services/information/tracking');
var dataService = require('./services/strategy/data');

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({ 
    cookie: { maxAge: 60000 },
    store: new session.MemoryStore,
    saveUninitialized: true,
    resave: 'true',
    secret: 'secret'
}))

app.use(flash());

app.use('/', indexRouter);
app.use('/dashboard', dashboardRouter);
app.use('/track', trackRouter);

app.use('/tracking', trackingService);
app.use('/data', dataService);

app.use(function(req, res, next) {
  next(createError(404));
});

app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;