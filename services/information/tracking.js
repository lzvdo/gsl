var express = require('express');
var router = express.Router();
var dbConn = require('../../lib/db');

router.get('/', function (req, res, next) {
    let code = req.query.code;
    let errorMessage = "Nenhuma entrega encontrada com o código ";

    dbConn.query('SELECT * FROM entregas WHERE codigo_rastreio = ' + code + ' LIMIT 1', function (err, rows) {
        if (err || rows.length == 0) {
            res.status(404).json({
                error: errorMessage + code
            });
        } else {
            res.status(200).json({
                data: rows[0]
            });
        }
    });
});

module.exports = router;
