var mysql = require('mysql');
var connection = mysql.createConnection({
	host:'127.0.0.1',
	user:'root',
	password:'password',
	database:'GSL'
});
connection.connect(function(error){
	if(!!error) {
		console.log(error);
	}
});

module.exports = connection;