var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
  res.render('track', {error: false, data: []}); 
});

module.exports = router;
