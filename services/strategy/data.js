var express = require('express');
var router = express.Router();
var dbConn = require('../../lib/db');

router.get('/', function (req, res, next) {

  let type = req.query.type;
  let errorGeneratingDataMessage = "Erro ao gerar dados estatísticos";
  let errorIncorrectParameterMessage = "Parâmetro incorreto";
  let fullQuery = 'SELECT * FROM entregas';
  let statusQuery = 'SELECT status as Status,COUNT(status) as Quantidade FROM entregas GROUP BY status';
  let valorQuery = 'SELECT valor as Valor from entregas';
  let distanciaQuery = 'SELECT distancia as Distancia from entregas';


  if (type == 'FULL') {
    dbConn.query(fullQuery, function (err, rows) {
      mapResponse(res, err, rows, errorGeneratingDataMessage);
    });
  } else if (type == "STATUS") {
    dbConn.query(statusQuery, function (err, rows) {
      mapResponse(res, err, rows, errorGeneratingDataMessage);
    });
  } else if (type == "VALOR") {
    dbConn.query(valorQuery, function (err, rows) {
      mapResponse(res, err, rows, errorGeneratingDataMessage);
    });
  } else if (type == "DISTANCIA") {
    dbConn.query(distanciaQuery, function (err, rows) {
      mapResponse(res, err, rows, errorGeneratingDataMessage);
    });
  }
  else {
    res.json({
      error: errorIncorrectParameterMessage
    })
  }
});

function mapResponse(res, err, rows, errorMessage) {
  if (err || rows.length == 0) {
    res.status(404).json({
      error: errorMessage
    });
  } else {
    res.status(200).json({
      data: rows
    });
  }
}

module.exports = router;