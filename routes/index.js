var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
    res.render('login');
});

router.post('/', function (req, res, next) {

    const user = req.body.user;
    const password = req.body.password;

    if (user == "admin" && password == "admin") {
        res.render('dashboard');
    } else {
        req.flash('error', 'Usuário ou senha incorretos');
        res.render('login');
    }
});

module.exports = router;
